package com.petclinicproject.petclinic.services;

import com.petclinicproject.petclinic.model.Owner;


public interface OwnerService extends CrudService<Owner, Long> {

    Owner findByLastName(String lastName);

}
