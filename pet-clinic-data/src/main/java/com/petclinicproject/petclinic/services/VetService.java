package com.petclinicproject.petclinic.services;

import com.petclinicproject.petclinic.model.Vet;


public interface VetService extends CrudService<Vet, Long>{

}
