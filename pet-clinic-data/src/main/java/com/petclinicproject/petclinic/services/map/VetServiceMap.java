package com.petclinicproject.petclinic.services.map;

import com.petclinicproject.petclinic.model.Vet;
import com.petclinicproject.petclinic.services.CrudService;

import java.util.Set;

public class VetServiceMap extends AbstractMapService<Vet, Long> implements CrudService<Vet, Long> {

@Override
public Set<Vet> findAll() {
        return super.findAll();
        }

@Override
public void deleteByID(Long id) {
        super.deleteByID(id);
        }

@Override
public void delete(Vet vet) {
        super.delete(vet);
        }

@Override
public Vet save(Vet vet) {
        return super.save(vet.getId(),vet);
        }

@Override
public Vet findById(Long id) {
        return super.findById(id);
        }
}
