package com.petclinicproject.petclinic.services;

import com.petclinicproject.petclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long>{
}
